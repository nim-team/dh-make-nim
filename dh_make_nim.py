#!/usr/bin/env python3

# Works end-to-end with: dashing seccomp pop3

import json
import os
import logging
import sys
import subprocess

from argparse import ArgumentParser

from deb_create_watch import detect_hosting_service, write_watch_file

control_tpl = """\
Source: {pkg_name}
Maintainer: Debian Nim Team <team+nim@tracker.debian.org>
Uploaders: {uploader}
Section: {section}
Priority: optional
Build-Depends:
 {build_depends}
Standards-Version: 4.5.0
Homepage: {homepage}
Vcs-Browser: https://salsa.debian.org/nim-team/{pkg_name}
Vcs-Git: https://salsa.debian.org/nim-team/{pkg_name}.git
Rules-Requires-Root: no

Package: {pkg_name}
Architecture: any
Depends: ${{misc:Depends}}
Description: {desc} - binaries
 {desc}

Package: {deblibname}
Architecture: all
Depends: ${{misc:Depends}}
Suggests: nim
Description: {desc} development library for Nim
 {desc}

Package: {pkg_name}-doc
Architecture: all
Section: doc
Depends: ${{misc:Depends}}
Description: {desc} - documentation
 {desc}
 This package contains documentation
"""

# TODO: use nim doc --git.url:{homepage} --git.commit:$v to link to local src?
rules_tpl = """\
#!/usr/bin/make -f
export DH_VERBOSE=1
include /usr/share/dpkg/default.mk

%:
	dh $@

override_dh_auto_build:
	#nimble build --verbose
	#nim c -d:release --noBabelPath {main_file}
	echo "generating HTML docs"
ifeq (,$(findstring nodoc,$(DEB_BUILD_OPTIONS)))
	rm htmldocs -rf
	nim doc -o:./htmldocs --project --index:on {main_file}
	nim buildIndex -o:htmldocs/theindex.html htmldocs
	find htmldocs -name '*.idx' -delete
endif

override_dh_auto_test:
	# Broken, see https://github.com/nim-lang/nimble/issues/637 and 638
	#http_proxy='http://127.0.0.1:9/' https_proxy='http://127.0.0.1:9/' \
	#	nimble test --verbose
	true

"""

doc_base_tpl = """\
Document: {debdocname}
Title: {desc}
Author: CHANGEME
Abstract: {desc}
Section: Programming/Nim

Format: HTML
Index: /usr/share/doc/{debsrcname}/htmldocs/theindex.html
Files: /usr/share/doc/{debsrcname}/htmldocs/*
"""

log = logging.getLogger()


def setup_logging(debug):
    lvl = logging.DEBUG if debug else logging.INFO
    log.setLevel(lvl)
    ch = logging.StreamHandler()
    ch.setLevel(lvl)
    formatter = logging.Formatter("%(message)s")
    ch.setFormatter(formatter)
    log.addHandler(ch)


def parse_args():
    ap = ArgumentParser()
    ap.add_argument("nimname", help="Nim package name")
    ap.add_argument("--deb-src-name", help="Debian source package name")
    ap.add_argument("-d", "--debug", action="store_true", help="Debug mode")
    ap.add_argument(
        "--no-create-repo", action="store_true", help="Do not create git repo"
    )
    ap.add_argument("--no-build", action="store_true", help="Do not build")
    return ap.parse_args()


def run_cmd(*a):
    log.debug("$ %s", " ".join(a))
    subprocess.check_call(list(a))


def load_package_data(name, fn):
    with open(fn) as f:
        j = json.load(f)
    for p in j:
        if p["name"] == name:
            return p
    log.error("Package not found")
    sys.exit(1)


def write(fn, c):
    log.info("Writing %s", fn)
    log.debug("------------------------")
    log.debug(c)
    log.debug("------------------------")
    with open(fn, "w") as f:
        f.write(c)


def detect_main_file(nimname):
    fn = nimname + ".nim"
    candidates = (fn, os.path.join("src", fn), os.path.join(nimname, fn))
    for fn in candidates:
        if os.path.isfile(fn):
            return fn


def split_requires(val):
    d = {}
    for r in val.split(", "):
        if r.startswith("https://") or r.startswith("http://"):
            log.warn("Dependency requires URL " + r)
        else:
            chunks = r.split()
            name = chunks[0]
            if len(chunks) == 1:
                d[name] = {}
            else:
                d[name] = tuple(chunks[1:])
    return d


def run_nimble_dump():
    log.info("Extracting data from nimble dump")
    # https://github.com/nim-lang/nimble/issues/640
    lines = subprocess.check_output("nimble dump -y", shell=True)
    lines = lines.decode().splitlines()
    d = {}
    for l in lines:
        # examples:
        # author: "foo"
        # requires: "nim >= 0.15.0, regex >= 0.7.4"
        try:
            name, val = l.split(": ")
            val = val.strip('"')
        except:
            continue
        if name != "requires":
            d[name] = val
        else:
            d[name] = split_requires(val)
    return d


def lib_pkg_name(name):
    if not name.startswith("nim"):
        name = "nim-" + name
    return name + "-dev"


def detect_build_deps(d):
    log.info("Detecting build deps from nimble dump")
    build_depends = ["debhelper-compat (= 12)", "nim"]
    for depname, depval in d["requires"].items():
        if depname == "nim":
            continue
        name = lib_pkg_name(depname)
        if not depval:
            build_depends.append(name)
        else:
            build_depends.append("{} ({} {})".format(name, depval[0], depval[1]))

    build_depends = ",\n ".join(sorted(build_depends))
    return build_depends


def create_repo(debsrcname, pkg_data, args):
    log.info("Creating Git repository")
    os.mkdir(debsrcname)
    os.chdir(debsrcname)
    run_cmd("git", "init", ".")

    log.debug("Creating debian directory")
    os.mkdir("debian")

    _, watch = detect_hosting_service(pkg_data["web"], args.nimname)

    log.info("Generating watch file")
    write_watch_file(watch)
    if args.debug:
        log.debug("-" * 10)
        with open("debian/watch") as f:
            for line in f:
                log.debug(line.rstrip())

        log.debug("-" * 10)

    log.debug("Generating temporary changelog file")
    run_cmd(
        "/usr/bin/dch", "--create", "--package", debsrcname, "--empty", "-v", "0.0.0-1"
    )

    if args.debug:
        log.debug("Running uscan test")
        run_cmd("/usr/bin/uscan", "--report")

    log.info("Importing tarball using uscan")
    run_cmd("/usr/bin/gbp", "import-orig", "--uscan", "-v", "--no-interactive")

    log.debug("Deleting temporary changelog")
    os.unlink("debian/changelog")


def main():
    args = parse_args()
    setup_logging(args.debug)
    nimname = args.nimname
    fn = os.path.expanduser("~/.nimble/packages_official.json")
    try:
        pkg_data = load_package_data(nimname, fn)
    except KeyError:
        log.error("Package not found")
        sys.exit(1)

    if pkg_data["method"] != "git":
        log.error("Method %s not supported" % pkg_data["method"])
        sys.exit(1)

    uploader = "{} <{}>".format(os.environ["DEBFULLNAME"], os.environ["DEBEMAIL"])

    is_lib = True
    if is_lib:
        section = "devel"
        if nimname.startswith("nim"):
            debsrcname = nimname
        else:
            debsrcname = "nim-" + nimname
    else:
        section = "utils"
        debsrcname = nimname
    # debsrcname = debsrcname.replace('_', '-')

    log.info("Debian source package name: %s", debsrcname)

    if not args.no_create_repo:
        create_repo(debsrcname, pkg_data, args)

    else:
        # assume the repo exists
        os.chdir(debsrcname)

    # generate debian/ files

    nimbledump = run_nimble_dump()
    build_depends = detect_build_deps(nimbledump)
    deblibname = lib_pkg_name(nimname)

    control = control_tpl.format(
        desc=pkg_data["description"],
        homepage=pkg_data["web"],
        pkg_name=debsrcname,
        deblibname=deblibname,
        uploader=uploader,
        section=section,
        build_depends=build_depends,
    )
    write("debian/control", control)

    main_file = detect_main_file(nimname)
    log.info("Main file: %s", main_file)

    rules = rules_tpl.format(main_file=main_file)
    write("debian/rules", rules)

    install = """#debian/*/bin/* /usr/bin"""
    write("debian/{}.install".format(debsrcname), install)

    install_lib = "src/* usr/share/nimble/{}".format(nimname)
    write("debian/{}.install".format(deblibname), install_lib)

    # documentation

    debdocname = debsrcname + "-doc"
    write("debian/{}.docs".format(debdocname), "htmldocs")

    doc_base = doc_base_tpl.format(
        desc=pkg_data["description"],
        homepage=pkg_data["web"],
        debdocname=debdocname,
        debsrcname=debsrcname,
    )
    write("debian/{}.doc-base".format(debdocname), doc_base)

    # detect version
    tags = subprocess.check_output("git --no-pager tag -l", shell=True)
    tags = tags.decode().split()
    tags = [t[9:] for t in tags if t.startswith("upstream/")]
    version = tags[0]
    log.info("Version: %s", version)

    try:
        os.unlink("debian/changelog")
    except FileNotFoundError:
        pass
    run_cmd(
        "/usr/bin/dch",
        "--create",
        "--package",
        debsrcname,
        "-v",
        version + "-1",
        "Initial release. (Closes: #XXXXXX)",
    )

    log.debug("Adding debian directory to Git")
    run_cmd("/usr/bin/git", "add", "debian")

    if not args.no_build:
        log.info("Building")
        run_cmd(
            "/usr/bin/dpkg-buildpackage", "-us", "-uc", "-b", "--no-check-builddeps"
        )
        try:
            run_cmd("lintian", "-iIE", "--pedantic")
        except:
            pass


if __name__ == "__main__":
    main()
